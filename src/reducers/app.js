const FETCH_SUCCESS = 'app/FETCH_SUCCESS';
const SHOW_PREVIOUS = '/app/SHOW_PREVIOUS';

const initialState = {
    items: [],
    data : "abcd"
};

// Reducer

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_SUCCESS:
            return {
                ...state,
                items: action.result.data
            };
        case SHOW_PREVIOUS:
            return {
                ...state,
                showPreviousButton: action.showPreviousButton
            }
        default:
            return state;
    }
}

// Actions

export function showPrevious(shouldShow) {
    return {
        type: SHOW_PREVIOUS,
        showPreviousButton: shouldShow
    }
}
