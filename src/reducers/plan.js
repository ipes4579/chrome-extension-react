const FETCH = 'plan/FETCH';
const FETCH_SUCCESS = 'plan/FETCH_SUCCESS';
const FETCH_FAIL = 'plan/FETCH_FAIL';

const initialState = {
    plan: {}
};

// Reducer

export default function reducer(state = initialState, action) {
    switch (action.type) {
        case FETCH_SUCCESS:
            return {
                ...state,
                plan: action.result.data
            };
        case FETCH_FAIL:
            return {
                ...state,
                plan: JSON.stringify(action)
            };
        default:
            return state;
    }
}

// Actions

export function getPlan() {
    return {
        types: [FETCH, FETCH_SUCCESS, FETCH_FAIL],
        // promise: client => client.get('https://datazen.katren.ru/calendar/day/2018-06-09/')
        promise: client => client.get('http://workinghours.skplanet.com/main/api/searchTodayPlan')
    };
}
