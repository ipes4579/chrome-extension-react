import { combineReducers } from 'redux';
import app from './app';
import mode from './mode';
import plan from './plan';

export default combineReducers({
  app,
  mode,
  plan
});
