const INCREASE = 'mode/INCREASE';
const DECREASE = 'mode/DECREASE';

const initialState = {
  mode: 0
};

// Reducer

export default function modeReducer(state = initialState, action) {
  switch (action.type) {
    case INCREASE:
      if(state.mode >= 2){
        return state;
      }
      return {
        ...state,
        mode: state.mode + 1
      };
    case DECREASE:
      if(state.mode <= 0){
        return state;
      }
      return {
        ...state,
        mode: state.mode - 1
      };
    default:
      return state;
  }
}

// Actions

export function increaseMode() {
  return  {
    type: INCREASE
  };
}
export function decreaseMode() {
  return  {
    type: DECREASE
  };
}
