import reducer from '../reducers';
import { createStore, applyMiddleware, compose } from 'redux';
import DevTools from './devtools';
import promiseMiddleware from './promiseMiddleware';

const middlewares = process.env.NODE_ENV === 'development' ?
    [applyMiddleware(promiseMiddleware), DevTools.instrument()] :
    [applyMiddleware(promiseMiddleware)];

var initialize = (initialState = {}) => {
    const store = createStore(reducer, initialState, compose(...middlewares));

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextReducer = require('../reducers');
            store.replaceReducer(nextReducer);
        });
    }
    return store;
};

export default initialize;

