import React, {Component} from 'react';
import logo from './logo.svg';
import './App.css';
import {connect} from 'react-redux';
import {getPlan} from './reducers/plan'

class App extends Component {

    componentDidMount() {
        this.props.getPlan();
    }

    renderPlan() {
        if (!this.props.plan || !this.props.plan.employeeVo) {
            return <div>Loading...</div>
        }
        return (
            <div>
                {this.props.plan.employeeVo.employeeNo}
            </div>
        )
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-logo" alt="logo"/>
                    <h1 className="App-title">Welcome to React</h1>
                </header>
                <div className="App-intro">
                    {this.props.data} To get started, edit <code>src/App.js</code> and save to reload.2
                </div>
                {this.renderPlan()}
            </div>
        );
    }
}

export default connect(
    state => ({
        data: state.app.data,
        plan: state.plan.plan
    }), {getPlan}
)(App);
